import { Pokemon } from '../types'
import { FETCH_POKEMONS_PENDING, FETCH_POKEMONS_SUCCESS, FETCH_POKEMONS_ERROR, PokemonActionTypes } from '../actions/actionTypes'

export interface PokemonsState {
    pending: boolean,
    pokemons: Pokemon[],
    error: string | null
}

const initialState = {
  pending: false,
  pokemons: [
          // {id: 1, name: 'bulbasaur', sprite:'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png', weight: 69, height: 7},
          // {id: 2, name: 'ivysaur', sprite:'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/2.png', weight: 130, height: 10},
          // {id: 3, name: 'venusaur', sprite:'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/3.png', weight: 1000, height: 20},
          // {id: 4, name: 'charmander', sprite:'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/4.png', weight: 85, height: 6},
          // {id: 5, name: 'charmeleon', sprite:'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/5.png', weight: 190, height: 11}
      ],
  error: null
}

export const pokemonsReducer = (state:PokemonsState = initialState, action: PokemonActionTypes) : PokemonsState => {
  switch(action.type){
    case FETCH_POKEMONS_PENDING: {
      return {
        ...state,
        pending: true
    }
    }
    case FETCH_POKEMONS_SUCCESS: {
      return {
        ...state,
        pending: false,
        pokemons: action.payload
      }
    }
    case FETCH_POKEMONS_ERROR: {
      return {
        ...state,
        pending: false,
        error: action.error
      }
    }
    default:
      return state
  }
}

export const getPokemons = (state: PokemonsState) => state.pokemons;
export const getPokemonsPending = (state: PokemonsState) => state.pending;
export const getPokemonsError = (state: PokemonsState) => state.error;
