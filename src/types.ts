export interface Pokemon {
    id: number;
    name: string;
    sprite: string;
    weight: number;
    height: number;
}
