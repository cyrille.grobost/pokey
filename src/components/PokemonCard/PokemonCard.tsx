// eslint-disable-next-line
import React from 'react'
import { Pokemon } from '../../types'
const PokemonCard = ({id, name, sprite, weight, height }: Pokemon) => {
    return (
        <div>
            <img alt={name} src={sprite}></img>
            <div>{name}</div>
            <div>{id}</div>
            <div>{weight}</div>
            <div>{height}</div>
        </div>
    )
}

export default PokemonCard
