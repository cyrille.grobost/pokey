import { FETCH_POKEMONS_PENDING, FETCH_POKEMONS_SUCCESS, FETCH_POKEMONS_ERROR, PokemonActionTypes } from "./actionTypes";
import { Pokemon } from '../types';

export const fetchPokemonsPending = (): PokemonActionTypes => ({
    type: FETCH_POKEMONS_PENDING,
  }
);

export const fetchPokemonsSuccess = (pokemons: Pokemon[] ): PokemonActionTypes => (
  {
    type: FETCH_POKEMONS_SUCCESS,
    payload: pokemons
  }
);

export const fetchPokemonsError = (error: string ) : PokemonActionTypes => (
  {
    type: FETCH_POKEMONS_ERROR,
    error: error
  }
);
