
import { Pokemon } from '../types';

export const FETCH_POKEMONS_PENDING = 'FETCH_POKEMONS_PENDING';
export const FETCH_POKEMONS_SUCCESS = 'FETCH_POKEMONS_SUCCESS';
export const FETCH_POKEMONS_ERROR = 'FETCH_POKEMONS_ERROR';


interface FetchPokemonsPendingAction {
  type: typeof FETCH_POKEMONS_PENDING
}

interface FetchPokemonsSuccessAction {
  type: typeof FETCH_POKEMONS_SUCCESS
  payload: Pokemon[]
}

interface FetchPokemonsErrorAction {
  type: typeof FETCH_POKEMONS_ERROR
  error: string
}

export type PokemonActionTypes = FetchPokemonsPendingAction | FetchPokemonsSuccessAction | FetchPokemonsErrorAction;
