// eslint-disable-next-line
import React, { useEffect } from 'react';
import PokemonCard from './components/PokemonCard'
import { useSelector, useDispatch } from 'react-redux';
import { PokemonsState, getPokemons, getPokemonsError, getPokemonsPending } from './reducers/pokemonsReducer';
import {fetchPokemonsPending, fetchPokemonsSuccess, fetchPokemonsError } from './actions/actions'

import './App.css';

function App() {
  const pokemons = useSelector<PokemonsState, PokemonsState['pokemons']>(
    (state) => getPokemons(state)
  );
  const pending = useSelector<PokemonsState, PokemonsState['pending']>(
    (state) => getPokemonsPending(state)
  );

  const error = useSelector<PokemonsState, PokemonsState['error']>(
    (state) => getPokemonsError(state)
  );

  const dispatch = useDispatch();

  interface PokemonApiResult {
    name: string;
    url: string;
  }

  function fetchData() {
    dispatch(fetchPokemonsPending());
    fetch('https://pokeapi.co/api/v2/pokemon/')
      .then(response => response.json())
      .then(data => {
        let results:PokemonApiResult[] = data.results;
        let promisesArray = results.map(async result => {
          return fetch(result.url).then(response => response.json());
        })
        return Promise.all(promisesArray);
      }).then(data => {
       const pokemons = data.map(p => {
        console.log(p);
         return {id:p.id, name:p.name, sprite: p.sprites.back_default, weight: p.weight, height: p.height};
        });
        dispatch(fetchPokemonsSuccess(pokemons));
        }).catch(err => {
          console.log(err)
          dispatch(fetchPokemonsError('error occured 2'));
        });
  }

    useEffect(fetchData, []);

  if(pending) {
    return (
      <div>Pending data...</div>
    );
  } else if(error) {
    return (
      <div>Pending data...</div>
    );
  } else {
    return (
      <div className="App">
        {pokemons.map( ({ id, name, sprite, height, weight }) => (
      <PokemonCard key={id} id={id} name={name} sprite={sprite} height={height} weight={weight}></PokemonCard>
    ))}
      </div>
    );
  }
}

export default App;
